.PHONY: all
all: checkout gitpull up

.PHONY: checkout
checkout: tequila passport-tequila

.PHONY: up
up:
	docker-compose up --build -d

.PHONY: exec
exec:
	docker exec -it wiki bash

.PHONY: psql
psql:
	docker exec -it postgres bash -c "psql wiki -U wikijs"

tequila:
	git clone git@gitlab.epfl.ch:si-idevfsd/wikijs-tequila.git $@

passport-tequila:
	git clone -b feature/actual-passport-api git@gitlab.com:epfl-idevfsd/passport-tequila.git

_find_git_depots := find . -name .git -prune |xargs -n 1 dirname

.PHONY: gitstatus
gitstatus:
	@set -e; for dir in `$(_find_git_depots)`; do (cd $$dir; echo "$$(tput bold)$$dir$$(tput sgr0)"; git status -uno; echo); done

.PHONY: gitpull
gitpull:
	@set -e; for dir in `$(_find_git_depots)`; do (cd $$dir; echo "$$(tput bold)$$dir$$(tput sgr0)"; git pull --autostash; echo); done

.PHONY: realclean
realclean:
	docker-compose down
	rm -rf db-data tequila passport-tequila
