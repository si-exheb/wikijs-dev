# WikiJS demo

This page list some of the possibilies of WikiJS.

## Abbreviations

*[HTML]: Hyper Text Markup Language
*[W3C]:  World Wide Web Consortium
The HTML specification
is maintained by the W3C.

## Emoji

`:apple:` will produce 🍎

## Footnote

Here is a footnote reference,[^1] and another.[^longnote]

[^1]: Here is the footnote.

[^longnote]: Here's one with multiple blocks.

    Subsequent paragraphs are indented to show that they
belong to the previous footnote.

## Image size

![EPFL aerial small](https://www.epfl.ch/about/wp-content/uploads/2020/01/EPFL-vue-aerienne-1536x864.jpg =200x100)

## Katex

### Simple

$\sqrt{3x-1}+(1+x)^2$

### Multi lines

$$\begin{array}{c}

\nabla \times \vec{\mathbf{B}} -\, \frac1c\, \frac{\partial\vec{\mathbf{E}}}{\partial t} &
= \frac{4\pi}{c}\vec{\mathbf{j}}    \nabla \cdot \vec{\mathbf{E}} & = 4 \pi \rho \\

\nabla \times \vec{\mathbf{E}}\, +\, \frac1c\, \frac{\partial\vec{\mathbf{B}}}{\partial t} & = \vec{\mathbf{0}} \\

\nabla \cdot \vec{\mathbf{B}} & = 0

\end{array}$$


## PlantUML

```plantuml
skinparam monochrome true
skinparam ranksep 20
skinparam dpi 150
skinparam arrowThickness 0.7
skinparam packageTitleAlignment left
skinparam usecaseBorderThickness 0.4
skinparam defaultFontSize 12
skinparam rectangleBorderThickness 1

rectangle "Main" {
  (main.view)
  (singleton)
}
rectangle "Base" {
  (base.component)
  (component)
  (model)
}
rectangle "<b>main.ts</b>" as main_ts

(component) ..> (base.component)
main_ts ==> (main.view)
(main.view) --> (component)
(main.view) ...> (singleton)
(singleton) ---> (model)
```

## mermaid

### graph TD

```mermaid
graph TD
  A[ Anyone ] -->|Can help | B( Go to github.com/yuzutech/kroki )
  B --> C{ How to contribute? }
  C --> D[ Reporting bugs ]
  C --> E[ Sharing ideas ]
  C --> F[ Advocating ]
```

### sequenceDiagram

```mermaid
sequenceDiagram
    participant Alice
    participant Bob
    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->>John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail!
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!
```

### Gantt

```mermaid
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           : a1, 2014-01-01, 30d
    Another task     : after a1, 20d
    section Another
    Task in sec      : 2014-01-12, 12d
    another task      : 24d
```

### Pie chart

```mermaid
pie
    title Key elements in Product X
    "Calcium" : 42.96
    "Potassium" : 50.05
    "Magnesium" : 10.01
    "Iron" : 5
```

## Subscript/Superscript

H~2~0
Exp^10^

## Tasklist

- [ ] Item 1
- [ ] Item 2
- [x] Item 3

## Tables

| testA | testB | testC |
| ----- | :---: | ----: |
| a     | b     | c     |

## Blockquotes

> Lorem ipsum dolor sit amet
> Consectetur adipiscing elit

> info
{.is-info}

> success
{.is-success}

> warning 
{.is-warning}

> daner
{.is-danger}

## Code Blocks syntax highlighting

```javascript
function lorem (ipsum) {
    const dolor = 'consectetur adipiscing elit'
}
```

## Content tabs

# Tabs {.tabset}
## First Tab

Any content here will go into the first tab...

## Second Tab

Any content here will go into the second tab...

## Third Tab

Any content here will go into the third tab...


Another title
=============

lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum 

## A collapsible section with markdown

<details>
  <summary>Click to expand!</summary>
  
  ## Heading
  1. A numbered
  2. list
     * With some
     * Sub bullets
</details>
